//
//  FCComboBox.m
//  FCComboBoxExample
//
//  Created by Franck Chevallier on 04/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FCComboBox.h"

@implementation FCComboBox
@synthesize textField = _textField;
@synthesize dataArray = _dataArray;
@synthesize message = _message;

- (id)initWithFrame:(CGRect)aFrame andDataArray:(NSArray *)anArray
{
    if (self = [super init]) {
        
        // We create a custom view size
        UIView *aView = [[UIView alloc] initWithFrame:aFrame];
        self.view = aView;
        self.view.backgroundColor = [UIColor clearColor];
        
        self.dataArray = anArray;
        
        // We have to call it manually because we've changed the view property
        [self loadView];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle
#define MARGIN 10
- (void)loadView
{
    // Add text field to the view within the size of the view
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    self.textField.delegate = self;
    
    [self.view addSubview:self.textField];
    
    // Add an arrow image
    UIImage *arrow = [UIImage imageNamed:@"arrow.png"];
    UIImageView *arrowView = [[UIImageView alloc] initWithImage:arrow];
    arrowView.center = CGPointMake(self.textField.bounds.size.width - MARGIN, self.textField.center.y);
    
    [self.view addSubview:arrowView];
    [arrowView release];
}

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];

    [_textField release];
    [_dataArray release];
    [_message release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UITextFieldDelegate
// Show the picker when one taps on the text field
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self showPickerView];
    return YES;
}

- (void)showPickerView
{
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    pickerView.showsSelectionIndicator = YES;
    pickerView.dataSource = self;
    pickerView.delegate = self;
    
    // Add a toolbar above the picker
    UIToolbar* toolbar = [[UIToolbar alloc] init];
    toolbar.barStyle = UIBarStyleBlackTranslucent;
    [toolbar sizeToFit];
    
    // Put the title and button in toolbar
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, toolbar.bounds.size.height)];
    label.backgroundColor = [UIColor clearColor];
    label.text = (self.message != nil) ? self.message : NSLocalizedString(@"Please make a choice", nil);
    label.textColor = [UIColor whiteColor];
    UIBarButtonItem *textButton = [[UIBarButtonItem alloc] initWithCustomView:label];
    
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"Done")
                                                                   style:UIBarButtonItemStyleDone target:self
                                                                  action:@selector(done:)];
    
    [toolbar setItems:[NSArray arrayWithObjects:textButton, flexibleSpaceLeft, doneButton, nil]];
    [textButton release];
    [flexibleSpaceLeft release];
    [doneButton release];
    
    // Display the combobox
    self.textField.inputView = pickerView;
    self.textField.inputAccessoryView = toolbar;
    
    [pickerView release];
    [toolbar release];
}

- (void)done:(id)sender
{
    [_textField resignFirstResponder];
}

#pragma mark - UIPickerviewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.dataArray count];
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.dataArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
{    
    self.textField.text = [self.dataArray objectAtIndex:row];
}

@end
