//
//  FCComboBox.h
//  FCComboBoxExample
//
//  Created by Franck Chevallier on 04/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FCComboBox : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

- (id)initWithFrame:(CGRect)aFrame andDataArray:(NSArray *)anArray;

@property (strong, nonatomic) NSArray *dataArray;
@property (strong, nonatomic) UITextField *textField;
@property (strong, nonatomic) NSString *message;

- (void)showPickerView;
- (void)done:(id)sender;

@end
