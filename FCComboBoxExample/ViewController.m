//
//  ViewController.m
//  FCComboBoxExample
//
//  Created by Franck Chevallier on 04/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController
@synthesize combo1 = _combo1, combo2 = _combo2;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Add a comboBox and init it with Frame and Data
    _combo1 = [[FCComboBox alloc] initWithFrame:CGRectMake(10, 50, 150, 30)
                                   andDataArray:[NSArray arrayWithObjects:@"Car", @"Plane", @"Train", nil]];
    
    // Customize the comboBox
    self.combo1.textField.borderStyle = UITextBorderStyleLine;
    self.combo1.textField.backgroundColor = [UIColor redColor];
    
    // Add a comboBox and init it with Frame and Data
    _combo2 = [[FCComboBox alloc] initWithFrame:CGRectMake(10, 100, 100, 30)
                                   andDataArray:[NSArray arrayWithObjects:@"Red", @"Blue", @"Green", @"White", nil]];
    
    // Customize the comboBox
    self.combo2.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.combo2.textField.backgroundColor = [UIColor blueColor];
    self.combo2.textField.textColor = [UIColor whiteColor];
    self.combo2.message = @"Make a choice";
    
    [self.view addSubview:self.combo1.view];
    [self.view addSubview:self.combo2.view];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    [_combo1 release];
    [_combo2 release];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)readComboBox:(id)sender
{
    NSLog(@"combo1 : %@", self.combo1.textField.text);
    NSLog(@"combo2 : %@", self.combo2.textField.text);
}

@end
