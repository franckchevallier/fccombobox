//
//  ViewController.h
//  FCComboBoxExample
//
//  Created by Franck Chevallier on 04/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FCComboBox.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) FCComboBox *combo1;
@property (strong, nonatomic) FCComboBox *combo2;

- (IBAction)readComboBox:(id)sender;

@end